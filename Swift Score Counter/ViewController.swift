//
//  ViewController.swift
//  Swift Score Counter
//
//  Created by Alex Brown on 7/2/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    //Bar items
    
    
    
    @IBAction func doneBtn(sender: AnyObject) {
        
        team1Lbl.text = "Team 1"
        team1Lbl.font = team1Lbl.font.fontWithSize(20)

        team2Lbl.text = "Team 2"
        team2Lbl.font = team2Lbl.font.fontWithSize(20)

        
        team1ScoreLbl.text = "Score"
        team1ScoreLbl.font = team1ScoreLbl.font.fontWithSize(20)

        team2ScoreLbl.text = "Score"
        team2ScoreLbl.font = team1ScoreLbl.font.fontWithSize(20)

        
        team1Stepper.value = 0
        team2Stepper.value = 0
        
    }
    
    
    @IBAction func addBtn(sender: AnyObject) {
        
        team1Lbl.text = team1Txt.text
        team1Lbl.font = team1Lbl.font.fontWithSize(30)
        
        team2Lbl.text = team2Txt.text
        team2Lbl.font = team2Lbl.font.fontWithSize(30)

        
        team1Txt.text = ""
        team2Txt.text = ""
        
        team1Txt.placeholder = "-Enter Text-"
        team2Txt.placeholder = "-Enter Text-"
    }
    
    //Team 1
   
    @IBOutlet var team1Lbl: UILabel!
    
    @IBOutlet var team1Txt: UITextField!
    
    @IBOutlet var team1ScoreLbl: UILabel!
    
    
    @IBOutlet var team1Stepper: UIStepper!
    @IBAction func team1Stepper(sender: UIStepper) {
        
            team1ScoreLbl.text = Int(sender.value).description
            team1ScoreLbl.font = team1ScoreLbl.font.fontWithSize(120)

        if (sender.value >= 21)
        {
            var alert = UIAlertController(title: "Congratulations", message: "You Won", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    //Team 2
    
    @IBOutlet var team2Lbl: UILabel!
    
    @IBOutlet var team2Txt: UITextField!
    
    @IBOutlet var team2ScoreLbl: UILabel!
    
    
    @IBOutlet var team2Stepper: UIStepper!
    @IBAction func team2Stepper(sender: UIStepper) {
        
        team2ScoreLbl.text = Int(sender.value).description
        team2ScoreLbl.font = team1ScoreLbl.font.fontWithSize(120)
        
        if (sender.value >= 21)
        {
            var alert = UIAlertController(title: "Congratulations", message: "You Won", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.team1Txt.delegate = self
        self.team2Txt.delegate = self
        
        var tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "DismissKeyboard")
        self.view.addGestureRecognizer(tap)
    
    }

    func DismissKeyboard(){
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        team1Txt.resignFirstResponder()
        team2Txt.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

